<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Aluno extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('alunos', function (Blueprint $table) {
            $table->increments('matricula');
            $table->integer('familia_id')->unsigned()->nullable();
            $table->string('nome', 200);
            $table->date('data_nascimento');
            $table->string('sexo',10);
            $table->string('bairro',250);
            $table->string('celular',14);
            $table->string('cep',10);
            $table->string('uf',2);
            $table->string('cidade',250);
            $table->string('cor');
            $table->string('data_emissao')->nullable();
            $table->string('email',150)->nullable();
            $table->string('folha')->nullable();
            $table->string('livro')->nullable();
            $table->string('nacionalidade',30)->nullable();
            $table->string('numero_residencia',10)->nullable();
            $table->string('registro_nascimento')->nullable();
            $table->string('rua',250);
            $table->string('status',10)->nullable();
            $table->string('telefone',14)->nullable();
            $table->foreign('familia_id')->references('id')->on('familias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos');
    }

}
