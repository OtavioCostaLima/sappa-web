@extends('templates.template')
@section('content')

        <div class="container">
            <p>
            <div class="card-header center teal darken-3">
                <i class="mdi mdi-calendar mdi-48px white-text"></i>
            </div>
            <div class="card-panel center">

                {!! Form::open(['route'=>'horario.store']) !!}
            <div class="input-field col s12 m4 l4">
                {!! Form::select('id_professor',$professores,null) !!}
                <label for="id_professor">Professor</label>
            </div>

            {!! Form::open(['route'=>'horario.store']) !!}
            <div class="input-field col s12 m4 l4">
                {!! Form::select('id_turma',$turmas,null) !!}
                <label for="id_turma">Turma</label>
            </div>

            <div class="input-field col s12 m6 l6">
                {!! Form::select('id_disciplina',$disciplinas,null) !!}
                <label for="id_disciplina">Disciplinas</label>
            </div>
            {!! Form::submit('Enviar',['class'=>'  btn']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection