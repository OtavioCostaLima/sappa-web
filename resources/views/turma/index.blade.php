@extends('templates.template')

@section('content')
    @if(isset($turmas))
        <table class="highlight centered">
            <thead>
            <tr>
                <th>Descrição</th>
                <th>Turno</th>
                <th>Ano</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>
            @foreach($turmas as $turma)
                <tr>
                    <td>{{$turma->descricao}}</td>
                    <td>{{$turma->turno}}</td>
                    <td>{{$turma->ano}}</td>

                    <td>
                      <a class="mdi mdi-sync mdi-24px" href="alunoturma"></a>
                      <a class="mdi mdi-account-edit mdi-24px" href="{{route('turma.edit',$turma->id)}}"></a>
                      <a class="mdi mdi-delete mdi-24px" href="{{route('turma.show',$turma->id)}}"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect green waves-purple" data-target="modal1"
           href="{{route('turma.create')}}"><i class="mdi mdi-plus"></i> </a>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
