<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Agendamento;

class Equipamento extends Model
{
  protected $fillable = ['descricao','quantidade'];

  public function agendamentos (){
    return $this->HasMany(App\Models\Agendamento::class);
  }
}
