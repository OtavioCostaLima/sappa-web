<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Aluno::class, function (Faker\Generator $faker) {
    return [
      'nome'=>$faker->name,
      'data_nascimento' =>$faker->dateTime($format = 'Y-m-d'),'sexo' =>'F',
      'bairro'=>$faker->country,'celular'=>$faker->e164PhoneNumber,
      'cep'=>$faker->postcode,
      'uf'=>$faker->stateAbbr,
      'cidade'=>$faker->city,
      'cor'=>'required',
      'data_emissao'=>$faker->dateTime($format = 'Y-m-d'),
      'email'=>$faker->unique()->safeEmail,
      'folha'=>'required',
      'livro'=>'required',
      'nacionalidade'=>'required',
      'numero_residencia'=>$faker->buildingNumber,
      'registro_nascimento'=>$faker->dateTime($format = 'Y-m-d'),
      'rua'=>$faker->streetName,
      'status'=>'ativo',
      'telefone'=>$faker->e164PhoneNumber
    ];
  });
