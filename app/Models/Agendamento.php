<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{

  public function familia(): BelongsTo {
      return $this->belongsTo(Familia::class);
  }
}
