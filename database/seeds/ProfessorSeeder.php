<?php

use Illuminate\Database\Seeder;
use App\Models\Professor;
use Faker;
class ProfessorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $fake =  new Generator();
    Professor::create([
        'user_id' =>$faker->unique()->integer,
        'nome' => $faker->name,
        'cpf',
        'bairro'=>$faker->region,
        'naturalidade',
        'nascionalidade',
        'sexo' => 'M',
        'email'=>$faker->unique()->safeEmail,
        'ctps',
        'data_emissao',
        'profissao',
        'data_nascimento',
        'estado_civil',
        'numero_residencia',
        'orgao_emissor',
        'pis_pasep',
        'pos_graduacao',
        'etinia',
        'grau_instrucao',
        'rg',
        'celular'=>$faker->mobileNumber,
        'endereco'=>$faker->streetName,
        'secao',
        'serie',
        'situacao',
        'titulo_eleitor',
        'complemento',
        'cep'=>$faker->postcode,
        'estado'=>$faker->stateAbbr,
        'cidade'=>$faker->city,
      ]);
    }
}
