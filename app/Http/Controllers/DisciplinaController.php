<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DisciplinaRequest;
use App\Models\Disciplina;
use App\Models\Horario;
use Gate;

class DisciplinaController extends Controller
{

    private $disciplina;

    function __construct(Disciplina $disciplina)
    {
        $this->disciplina = $disciplina;
    }

    public function listar()
    {
        $disciplinas = $this->disciplina->all();
        $response = [
            'tipos' => [
                'Reprova' => 'Reprova',
                'Não Reprova' => 'Não Reprova'
            ],
            'data' => $disciplinas
        ];
        return response()->json($response);
    }

    public function index()
    {
        $tipos = ['Reprova' => 'Reprova', 'Não Reprova' => 'Não Reprova'];
        $disciplinas = $this->disciplina->paginate(10);

        return view('disciplina.index', compact('disciplinas', 'tipos'));
    }

    public function create()
    {
        $title = 'Pagina Cadastro';
        $tipos = ['Reprova' => 'Reprova', 'Não Reprova' => 'Não Reprova'];
        return view('disciplina.index', compact('tipos', 'title'));
    }

    public function store(DisciplinaRequest $request)
    {
        $dados = $request->all();
        $insert = $this->disciplina->create($dados);

        if ($insert) {
            return redirect()->route('disciplinas.index');
        } else {
            return redirect()->route('disciplinas.store');
        }
    }

    public function show($id)
    {
        $dis = $this->disciplina->find($id);
        $delete = $dis->delete();
        if ($delete) {
            return redirect()->route('disciplinas.index');
        }
    }

    public function edit($id)
    {
        $tipos = ['Reprova' => 'Reprova', 'Não Reprova' => 'Não Reprova'];
        $disciplina = $this->disciplina->find($id);
        $title = "Editar: {{$disciplina->descricao}}";
        return view('disciplina.create', compact('disciplina', 'title', 'tipos'));
    }

    public function update(DisciplinaRequest $request, $id)
    {
        $dados = request()->except('_token', '_method');
        $disc = $this->disciplina->find($id);
        if ($disc->update($dados)) {
            return redirect()->route('disciplinas.index');
        } else {
            return redirect('disciplinas.edit');
        }
    }


/**
  Retorna as disciplinas que o professor não lecina em determinada turma!
Rota: disciplinas/professor/{idProfessor}/turma/{idTurma}/
*/
    public static function getDisciplinasHorario($idProfessor,$idTurma)
    {
        $ids_disciplinas = Horario::select('id_disciplina')->where('id_professor', '=', $idProfessor)->where('id_turma', '=',$idTurma)->get();
        return Disciplina::whereNotIn('id', $ids_disciplinas)->get();
    }

/**
  Retorna as discipinas de uma determinada turma!
  Rota: /disciplinas/turma/{id_turma}
*/
    public static function getDisciplinas($idTurma)
    {
        $ids_disciplinas = Horario::select('id_disciplina')->where('id_turma', '=', $idTurma)->get();
        return Disciplina::whereIn('id', $ids_disciplinas)->get();
    }

}
