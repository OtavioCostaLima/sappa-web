<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
  protected  $fillable = [
          'id_contratante',
          'matricula_aluno',
          'data_contrato',
          'data_renovacao'];

  public $rules = [
        'id_contratante' => 'required',
        'matricula_aluno'=>'required',
        'data_contrato'=>'required',
        'data_renovacao'=>'required'];

}
