@extends('templates.template')
@section('content')
    <div class="container">
        <p>
        <div class="card-header center teal darken-3">
            <i class="mdi mdi-face-profile mdi-48px white-text"></i>
        </div>
        <div class="card-panel center">
            {!! Form::open(['route'=>'horario.store']) !!}
            <div class="input-field col s12 m6 l6">

                <h3> {{$professor->nome}}</h3>
            </div>

            <div class="input-field col s12 m6 l6">
                {!! Form::text('id_professor',$professor->id,null,['id'=>'id_professor',]) !!}
            </div>

            <div class="input-field col s12 m6 l6">
                {!! Form::select('id_turma',$turmas,null,['id'=>'id_turma']) !!}
            </div>

            <div class="input-field col s12 m6 l6">
                {!! Form::select('id_disciplina',$disciplinas,null,['id'=>'id_disciplina']) !!}
            </div>


            {!! Form::submit('Enviar',['class'=>'  btn']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            $('#id_turma').change(function () {
                $.get("{{ url('/a')}}",
                    {option: $(this).val()},
                    function (data) {
                        var asas = $('#asas');
                        asas.empty();
                        var $elements = "";
                        $.each(data, function (index, element) {
                            $elements = $elements + "s";

                            $("#asas").append($elements).click(function () {
                                alert("test")
                            });

                        });
                    });
            });
        });

    </script>
@endsection
