<?php

use Illuminate\Database\Seeder;
use App\Models\TipoNota;
class TipoNotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoNota::insert(['descricao' => 'MENSAL']);
        TipoNota::insert(['descricao' => 'BIMESTRAL']);
        TipoNota::insert(['descricao' => 'QUALITATIVO']);
        TipoNota::insert(['descricao' => 'EXTRA']);
    }
}
