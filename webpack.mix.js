const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').js('resources/assets/js/utils.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


mix.copy('node_modules/materialize-css/dist/css/materialize.css', 'public/materialize/css/materialize.css');
mix.copy('node_modules/materialize-css/dist/js/materialize.js', 'public/materialize/js/materialize.js');
mix.copy('node_modules/materialize-css/dist/fonts', 'public/materialize/fonts/roboto');
mix.copy('node_modules/mdi', 'public/material-design-icones',false);
