# Sistema Acadêmico Distribuído!

`` O projeto ainda está em processo de incubação mas aceitamos contribuição de qualquer um que estiver interessado.``

---

Esse projeto que ainda está em desenvolvimento e refere-se a um sistema acadêmico baseado em modelo SAAS e Multi-Tenancy onde podemos adicionar novos serviços de acordo com as necessidades do **Tenancy** (Cliente)!


> O projeto defini-se em desenvolver um sistema que disponibilizará serviços online para escolas publicas e privadas que trabalhão com todas as modalidades de ensino.>


## Estes serviços vão desde serviços para gestão acadêmico como também:

* disponibilizar entre todas as escolas margens de desempenho de de uma escola em relação a outras 

* integração a informações das provas realizadas pelo MEC (Olimpíadas de Matemática, Prova Brasil, etc...)



*Tecnologias utilizadas:* __Laravel FrameWork, VueJS, Materialize, Vuetify__

[Meu Facebook](http://facebook.com/otavio.costa.lima "Otávio Costa")

email: <otavio_slz15@hotmail.com>
