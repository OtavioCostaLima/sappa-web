<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Turma;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**

Serie é o grau no qual o aluno pode está matriculado!
*/
class Serie extends Model {

    public function turmas(): HasMany {
        return $this->hasMany(Modalidade::class,'id');
    }



}
