<?php

Route::group(['middleware' => 'verifique'], function(){
Route::get('home', 'HomeController@index')->middleware('verifique');
});

Route::get('/', function () {
    return view('bem-vindo');
});


Route::get('/verifique/email',function(){
     return view('mails.verificacao-email');
})->name('verifique-email')->middleware('auth');


Route::get('/users/confirmation/{token}','Auth\RegisterController@confirmation')->name('confirmation');

Route::get('/mail','MailController@index');



Route::get('/responsavel', function () {
    return view('responsavel.index');
});

Route::resource('disciplinas', 'DisciplinaController');
Route::resource('turma', 'TurmaController');
Route::resource('alunoturma', 'AlunoTurmaController');
Route::resource('nota', 'NotaController');
Route::resource('professor', 'ProfessorController');
Route::resource('aluno', 'AlunoController');
Route::resource('funcionario', 'FuncionarioController');
Route::resource('horario', 'HorarioController');
Route::resource('serie', 'SerieController');
Route::resource('bimestre', 'BimestreController');
Route::resource('tiponotas', 'TipoNotaController');

Route::get('horario/turma/{turma}/professor/{professor}/disciplina/{disciplina}/', 'HorarioController@getHorario');
Route::get('getprofessores/{id_turma}/{id_disciplina}','ProfessorController@getProfessors');

Route::group(['prefix'=>'alunos'], function(){
  //Route::get('alunos/semturma','AlunoController@getAlunosNaoVinculados'); modificada
  Route::get('vinculo=false','AlunoController@getAlunosNaoVinculados');
  Route::get('vinculo=true','AlunoController@getAlunosVinculados');
});


Route::get('turmas','TurmaController@getTurmas');

Route::group(['prefix'=>'turma'], function(){
  Route::get('{id}/alunos','AlunoTurmaController@getAlunosTurma');
});

Route::group(['prefix'=>'disciplinas'], function(){
  //Route::get('/getdisciplinasNotIn','DisciplinaController@getDisciplinasHorario');  modificada
  Route::get('professor/{idProfessor}/turma/{idTurma}','DisciplinaController@getDisciplinasHorario');

  //Route::get('/getdisciplinas/{id_turma}','DisciplinaController@getDisciplinas'); modificada
  Route::get('turma/{idTurma}','DisciplinaController@getDisciplinas');
});

Route::group(['prefix'=>'dashboard'], function(){
  Route::get('administrador',function(){
    return view('dashboards.administrador.dashboard');
  });
  Route::get('professor',function(){
    return view('dashboards.professor.dashboard');
  });

  Route::get('aluno',function(){
    return view('dashboards.administrador.aluno');
  });

  Route::get('aluno/create',function(){
    return view('dashboards.aluno.create');
  });

});

Route::get('getalunos', 'AlunoController@getAlunos');

Auth::routes();
