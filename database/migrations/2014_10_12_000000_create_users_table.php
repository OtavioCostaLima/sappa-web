<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('confirmed')->default(0);
            $table->string('token',254)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        \App\User::create([
          'name' => 'Otávio Costa Lima',
          'email' => 'otavio_slz15@hotmail.com',
          'password' => bcrypt('123456789'),
          'remember_token' => str_random(10),
          'confirmed' => 1
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
