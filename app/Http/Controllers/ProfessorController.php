<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfessorResquest;
use App\Models\Professor;
use App\Models\Horario;
use App\User;
class ProfessorController extends Controller
{

    private $professor;

    function __construct(Professor $professor)
    {
          $this->middleware('auth');
        $this->professor = $professor;
    }

    public function index()
    {
        $professores = $this->professor->all();
        return view('professor.index', compact('professores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cadastro Professor';
        return view('professor.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessorResquest $request)
    {
      $usuario = new User();
      $usuario->name= $request->nome;
      $usuario->email = $request->email;
      $usuario->password = bcrypt('123456789');
      $usuario->confirmed = false;
      $usuario->save();

        if($usuario){
          $dados = $request->all();
          $professor = $this->professor->create($dados);
          $insert = $professor->user()->associate($usuario);
          $insert->save();
  
          if ($insert) {
              return redirect()->route('professor.index');
          } else {
              return redirect()->route('professor.store');
          }

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prof = $this->professor->find($id);
        $delete = $prof->delete();
        if ($delete) {
            return redirect()->route('professor.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professor = $this->professor->find($id);
        return view('professor.create', compact('professor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfessorResquest $request, $id)
    {
        $dados = request()->except('_token', '_method');
        $prof = $this->professor->find($id);
        if ($prof->update($dados)) {
            return redirect()->route('professor.index');
        } else {
            return redirect('professor.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function retornarTurmas()
    {
        $this->professor('horarios')->join('turmas', function ($join) {
            $join->on('horarios.id_professor', '=', 'professor.id');
        })
            ->get();
    }

    public static function getProfessors($id_Tur, $id_d)
    {
        $ids_professores = Horario::select('id_professor')->where('id_turma', '=', $id_Tur)->where('id_disciplina', '=', $id_d)->get();
        return Professor::whereIn('id', $ids_professores)->get();
    }


}
