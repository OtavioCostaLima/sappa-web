@extends('templates.template')
@if(isset($errors) && count($errors)>0)
    @foreach($errors->all() as $error)
  {{$error}}<br>
    @endforeach
@endif

@section('content')
    @if(isset($professor))
        {!! Form::model($professor,['route'=>['professor.update', $professor->id],'class'=>'form','method'=>'put'])!!}
    @else
        {!!Form::open(['route' =>'professor.store','novalidate'=>'novalidate','id'=>'form'])!!}
    @endif
    <div class="container" onload="Materialize.fadeInImage('#image-test')">
        <div class="card-panel">
            <div class="card-title">

            <span class="green-text text-darken-3"><h3>Cadastro Professor</h3>
        <p>Informe os dados</p></span>
            </div>
            <p>
            <div class="card-content">
                <div class="row">
                    <div class="input-field inline col s12 m6 l6">
                        <label for="nome">Nome Professor</label>
                        {!! Form::text('nome',null,['class'=>'validate input-field', 'aria-describedby'=>'basic-addon1','id'=>'nome','minlength'=>'10','required'])!!}
                        @if($errors->has('nome'))
                        <span class="red-text">{{$errors->first('nome')}}</span>
                        @endif

                    </div>

                    <div class="input-field col s6 m3 l3">
                        {!! Form::select('sexo', ['M' => 'Masculino', 'F' => 'Feminino'],null,['class'=>'validate ','id'=>'sexo'])!!}
                        <label for="sexo">Sexo</label>
                    </div>

                    <div class="input-field col s6 m3 l3">
                        {!!Form::text('data_nascimento',null,['class'=>'datepicker','id'=>'data_nascimento'])!!}
                        <label for="data_nascimento">Data Nascimento</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('cpf',null,['class'=>'validate','id'=>'cpf','onkeypress'=>'mascara(this,cpf)'])!!}
                        <label for="cpf">CPF</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('naturalidade',null,['class'=>'validate','id'=>'naturalidade'])!!}
                        <label for="naturalidade">Naturalidade</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('nascionalidade',null,['class'=>'validate','id'=>'nascionalidade'])!!}
                        <label for="nascionalidade">Nascionalidade</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('pis_pasep',null,['class'=>'validate','id'=>'pis_pasep'])!!}
                        <label for="pis_pasep">Pis/Pasep</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('grau_instrucao',null,['class'=>'validate','id'=>'grau_instrucao'])!!}
                        <label for="grau_instrucao">Grau</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('profissao',null,['class'=>'validate','id'=>'profissao'])!!}
                        <label for="profissao">Profissão</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('pos_graduacao',null,['class'=>'validate','id'=>'pos_graduacao'])!!}
                        <label for="pos_graduacao">Pos Graduacao</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('rg',null,['class'=>'validate','id'=>'rg'])!!}
                        <label for="rg">Identidade</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!! Form::select('orgao_emissor',[''=>'Selecione um Orgão','SSP'=>'SSP - Secretaria de Segurança Pública',
        'COREN'=>'COREN - Conselho Regional de Enfermagem',
        'CRA'=>'CRA - Conselho Regional de Administração',
        'CRAS'=>'CRAS - Conselho Regional de Assistentes Sociais',
        'CRB'=>'CRB - Conselho Regional de Biblioteconomia',
        'CRC'=>'CRC - Conselho Regional de Contabilidade',
        'CRE'=>'CRE - Conselho Regional de Estatística',
        'CREA'=>'CREA - Conselho Regional de Engenharia Arquitetura e Agronomia',
        'CRECI'=>'CRECI - Conselho Regional de Corretores de Imóveis',
        'CREFIT'=>'CREFIT - Conselho Regional de Fisioterapia e Terapia Ocupacional',
        'CRF'=>'CRF - Conselho Regional de Farmácia',
        'CRM'=>'CRM - Conselho Regional de Medicina',
        'CRN'=>'CRN - Conselho Regional de Nutrição',
        'CRO'=>'CRO - Conselho Regional de Odontologia',
        'CRP'=>'CRP - Conselho Regional de Psicologia',
        'CRPRE'=>'CRPRE - Conselho Regional de Profissionais de Relações Públicas',
        'CRQ'=>'CRQ - Conselho Regional de Química',
        'CRRC'=>'CRRC - Conselho Regional de Representantes Comerciais',
        'CRMV'=>'CRMV - Conselho Regional de Medicina Veterinária',
        'DPF'=>'DPF - Polícia Federal',
        'EST'=>'EST - Documentos Estrangeiros',
        'I CLA'=>'I CLA - Carteira de Identidade Classista',
        'MAE'=>'MAE - Ministério da Aeronáutica',
        'MEX'=>'MEX - Ministério do Exército',
        'MMA'=>'MMA - Ministério da Marinha',
        'OAB'=>'OAB - Ordem dos Advogados do Brasil',
        'OMB'=>'OMB - Ordens dos Músicos do Brasil',
        'IFP'=>'IFP - Instituto de Identificação Félix Pacheco',
        'OUT'=>'OUT - Outros Emissores'],null,['class'=>'validate','id'=>'orgao_emissor']) !!}
                        <label for="orgao_emissor">Emissor</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('data_emissao',null,['class'=>'datepicker','id'=>'data_emissao'])!!}
                        <label for="data_emissao">Data Emissão</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('etinia',null,['class'=>'validate','id'=>'etinia'])!!}
                        <label for="etinia">Etinia</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('ctps',null,['class'=>'validate','id'=>'ctps'])!!}
                        <label for="ctps">CTPS</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('email',null,['class'=>'validate','id'=>'email'])!!}
                        <label for="email">Email</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('celular',null,['class'=>'validate','id'=>'celular'])!!}
                        <label for="celular">Celular</label>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        {!!Form::text('cep',null,['class'=>'validate','id'=>'cep'])!!}
                        <label for="cep">CEP</label>
                    </div>


                    <div class="input-field col s6 m2 l2">
                        {!!Form::select('estado_civil',['solteira'=>'Solteira','casada'=>'Casada','separada'=>'Separada'],null,['class'=>'validate','id'=>'estado_civil'])!!}
                        <label for="estado_civil">Estado civil</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('endereco',null,['class'=>'validate','id'=>'endereco'])!!}
                        <label for="endereco">Endereço</label>
                    </div>

                    <div class="input-field col s12 m2 l2">
                        {!!Form::text('numero_residencia',null,['class'=>'validate','id'=>'numero_residencia'])!!}
                        <label for="numero_residencia">Número</label>
                    </div>

                    <div class="input-field col s12 m4 l4">
                        {!!Form::select('estado',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espirito Santo','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'Mato Grossodo Sul','MT'=>'Mato Grosso','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Riode Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['class'=>'validate','id'=>'estado'])!!}
                        <label for="estado">Estado</label>

                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('complemento',null,['class'=>'validate','id'=>'complemento'])!!}
                        <label for="complemento">Complemento</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('bairro',null,['class'=>'validate','id'=>'bairro'])!!}
                        <label for="bairro">Bairro</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                        {!!Form::text('cidade',null,['class'=>'validate','id'=>'cidade'])!!}
                        <label for="cidade">Cidade</label>
                    </div>

                    <button class="btn waves-effect waves-light direct right" type="submit" name="action">Enviar
                        <i class="mdi mdi-arrow-bottom-rightt"></i>
                    </button>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils.js")}}" type="text/javascript"></script>

@endsection
