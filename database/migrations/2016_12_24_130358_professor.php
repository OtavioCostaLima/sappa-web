<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Professor extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('user_id')->unsigned()->unique()->nullable();
            $table->string('cpf')->unique();
            $table->string('bairro');
            $table->string('sexo',1);
            $table->string('ctps')->nullable();
            $table->date('data_emissao');
            $table->date('data_nascimento');
            $table->string('estado_civil');
            $table->string('grau_instrucao');
            $table->string('numero_residencia');
            $table->string('orgao_emissor');
            $table->string('pis_pasep');
            $table->string('pos_graduacao')->nullable();
            $table->string('cidade');
            $table->string('etinia')->nullable();;
            $table->string('rg');
            $table->string('cep');
            $table->string('endereco');
            $table->string('celular');
            $table->string('naturalidade');
            $table->string('nascionalidade');
            $table->string('profissao');
            $table->string('complemento');
            $table->string('estado');
            $table->string('email');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //  $table->string('secao')->nulable(); ;
            //$table->string('serie')->nulable(); ;
            //$table->string('situacao');
            //$table->string('titulo_eleitor')->nulable(); ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professors');
    }

}
