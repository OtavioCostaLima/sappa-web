<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model {

    protected $fillable = ['descricao','tipo'];

    public $rules = [
      'descricao' => 'required',
      'tipo'  => 'required'
    ];

    public $timetamps = false;

}
