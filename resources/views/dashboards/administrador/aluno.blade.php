@extends('dashboards.templates.template')
<style media="screen">
  body {
    background-color: rgb(207, 221, 204);
  }

  .card-content {
    background-color: rgb(72, 185, 100);
    color: rgb(252, 252, 252);
    margin-top: 0px;
    text-align: center;
    font-size: 2em;
  }

  .card-title {
    background-color: rgb(43, 158, 72);
    color: rgb(252, 252, 252);
    text-align: center;
  }


</style>
@section('conteudo')
    <div class="row">
      <div class="col s12 m3 l3">
        <div class="card">
          <div class="card-title">
              todal de alunos
          </div>
          <div class="card-content">
        100
          </div>
        </div>
    </div>

    <div class="col s12 m3 l3">
        <div class="card">
          <div class="card-title">
            alunos vinculados
          </div>
          <div class="card-content">
          53
          </div>
        </div>
    </div>

    <div class="col s12 m3 l3">
        <div class="card">
          <div class="card-title">
          não vinculados
          </div>
          <div class="card-content">
1
      </div>
        </div>
    </div>

    <div class="col s12 m3 l3">
        <div class="card">
          <div class="card-title">
          todal de alunos
          </div>
          <div class="card-content">
            ssa
      </div>
        </div>
    </div>
  </div>
  <div class="fixed-action-btn">
      <a class="btn-floating btn-large waves-effect waves-light darken-4 waves-purple" data-target="modal1"
         href="{{url('/dashboard/aluno/create')}}"><i class="mdi mdi-plus"></i></a>
  </div>
@endsection
