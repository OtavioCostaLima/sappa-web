<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inicio</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
    body{
      background-image: url('icones/imagem.jpg');
      background-repeat: no-repeat;
    }

        html, body {
            background-color: #0f9d58;
            color: #FFEBEE;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #FFEBEE;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
    <link href="{{url("materialize/css/materialize.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{url("material-design-icones/css/materialdesignicons.css")}}" rel="stylesheet" type="text/css">
    </head>
<body onload="Materialize.fadeInImage('#nome_im')">
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Início</a>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    Sair
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Registrar</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md" id="nome_im">
        SAD
        </div>

        <div class="links">
            <a class="mdi mdi-facebook-box mdi-24px" href="">Facebook</a>
            <a class="mdi mdi-contact-mail mdi-24px" href="">Contato</a>
        </div>

    </div>

</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
<script>$('#modal1').modal();</script>
</body>


</html>
