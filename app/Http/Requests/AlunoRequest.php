<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlunoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'familia_id' => 'required',
          'nome'=>'required',
          'data_nascimento' =>'required',
          'sexo' =>'required',
          'bairro'=>'required',
          'celular'=>'required',
          'cep'=>'required',
          'uf'=>'required',
          'cidade'=>'required',
          'cor'=>'required',
          'data_emissao'=>'required',
          'email'=>'required|email',
          'folha'=>'required',
          'livro'=>'required',
          'nacionalidade'=>'required',
          'numero_residencia'=>'required',
          'registro_nascimento'=>'required',
          'rua'=>'required',
          'status'=>'required',
          'telefone'=>'required'
        ];
    }
}
