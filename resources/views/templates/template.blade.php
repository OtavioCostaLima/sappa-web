<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>Controle Impacto</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <link href="{{url("materialize/css/materialize.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{url("material-design-icones/css/materialdesignicons.css")}}" rel="stylesheet" type="text/css">
  @yield('css')
  <link rel="stylesheet" href="/css/style.css" type="text/css">

</head>

<body>
<style media="screen">
  body {
    background-color: rgb(207, 221, 204);
  }
</style>
@if(Auth::guest())
 {{abort(403, 'Ação não autorizada.')}}
@else
   @include('templates.nav-bar-menu')
  @yield('content')
@endif




@yield('scripts')

<script>
    $(".button-collapse").sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
    });
</script>

</body>


</html>
