@if (Route::has('login'))
    <div class="top-right links">
        @if (Auth::check())
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                Sair
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else
            <a href="{{ url('/login') }}">Login</a>
            <a href="{{ url('/register') }}">Registrar</a>
        @endif
    </div>
@endif

  @if (!Auth::guest())
    <h1> Sejá Bem-vindo {{Auth::user()->name}}.</h1>
    <h1>Você ainda precisa confirmar sua conta!</h1>
    <h3>Para isso enviamos um link para: {{Auth::user()->email}} para que possa confirmar</h3>
    @endif
{{Auth::logout()}}
