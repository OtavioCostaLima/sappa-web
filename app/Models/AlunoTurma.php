<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlunoTurma extends Model
{
  protected $table = 'aluno_turma';
  protected $fillable =  ['matricula_aluno','id_turma'];

  public function alunos() {
return $this->belongsToMany('App\Models\Turma', 'aluno_turma', 'id_turma', 'matricula_aluno')->withPivot(['numero_chamada']);
  }

}
