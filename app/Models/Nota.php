<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\TipoNota;
use App\Models\Aluno;
use App\Models\Bimestre;

class Nota extends Model {

protected $fillable = ['tipo_nota','valor','id_horario'];

 public $rules = ['tipo_nota' =>'required','valor'=>'required','id_horario'=>'required'];

    public function tipo_notas(): BelongsTo {

        return $this->belongsTo(TipoNota::class,'id');
    }

    public function notas(): BelongsTo {
        return $this->belongsTo(Aluno::class,'id');
    }

    public function bimestre(): BelongsTo {
        return $this->belongsTo(Bimestre::class,'id');
    }

}
