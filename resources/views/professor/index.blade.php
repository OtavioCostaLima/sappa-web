@extends('templates.template')

@section('content')
    <div class="container">
        <table class="highlight centered">
            <thead>
            <tr>
                <th>Nome</th>
                <th>sexo</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>

                <tr>
                    @foreach($professores as $professor)
                    <td>{{$professor->nome}}</td>
                    <td>{{$professor->sexo}}</td>
                        <td>
                            <a class="mdi mdi-account-edit mdi-24px" href="{{route('professor.edit',$professor->id)}}"></a>
                            <a class="mdi mdi-delete mdi-24px" href="{{route('professor.show',$professor->id)}}"></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large waves-effect waves-light darken-4 waves-purple" data-target="modal1" href="{{route('professor.create')}}"><i class="mdi mdi-plus"></i></a>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
