<?php

use Illuminate\Database\Seeder;
use App\Models\Aluno;
use Faker\Generator;

class AlunoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
          factory(Aluno::class,100)->create();
    }

}
