@extends('templates.template')
@section('content')
@if(isset($errors) && count($errors)>0)
<div class="alert">
  @foreach($errors->all() as $error)
  {{$error}}<br>
  @endforeach

</div>
  @endif
    <div class="container">

        <div class="container">
            <p>
            <div class="card-header center teal darken-3">
                <i class="mdi mdi-alphabetical mdi-48px white-text"></i>
            </div>

            <div class="card-panel center">
            @if(isset($turma))
                {!! Form::model($turma,['route'=>['turma.update', $turma->id],'class'=>'form','method'=>'put'])!!}
            @else
                {!!Form::open(['route'=>'turma.store'])!!}
            @endif
            <div class="row">
                <div class="input-field col s12 m6 l6">
                    {!! Form::text('descricao',null,['class'=>'validate','data-length'=>'10'])!!}
                    <label for="descricao">Turma</label>
                </div>
                <div class="input-field col s12 m6 l6">
                    {!! Form::text('ano',null,['class'=>'datepicker'])!!}
                    <label for="ano">Ano</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m6 l6">
                    {!! Form::select('turno',$turnos,null,['required'=>""])!!}
                    <label for="turno">Turno</label>
                </div>

                <div class="input-field col s12 m6 l6">
                    {!! Form::select('id_serie', $series,null,['class'=>'validate','required'=>""])!!}
                    <label for="id_serie">Series</label>
                </div>
            </div>

            {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}
            {!!Form::close()!!}


    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
