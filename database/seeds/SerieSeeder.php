<?php

use Illuminate\Database\Seeder;

class SerieSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      //\App\Models\Serie::truncate();

        \App\Models\Serie::create([
            'descricao' => '1 série',
            'tipo' => 'Fundamental I'

        ]);

        \App\Models\Serie::create([
            'descricao' => '2 série',
            'tipo' => 'Fundamental I'

        ]);

        \App\Models\Serie::create([
            'descricao' => '3 série',
            'tipo' => 'Fundamental I'

        ]);

        \App\Models\Serie::create([
            'descricao' => '4 série',
            'tipo' => 'Fundamental I'

        ]);

        \App\Models\Serie::create([
            'descricao' => '5 série',
            'tipo' => 'Fundamental I'

        ]);

        \App\Models\Serie::create([
            'descricao' => '6 série',
            'tipo' => 'Fundamental II'

        ]);

        \App\Models\Serie::create([
            'descricao' => '7 série',
            'tipo' => 'Fundamental II'

        ]);

        \App\Models\Serie::create([
            'descricao' => '8 série',
            'tipo' => 'Fundamental II'

        ]);

        \App\Models\Serie::create([
            'descricao' => '9 série',
            'tipo' => 'Fundamental II'

        ]);

        \App\Models\Serie::create([
            'descricao' => '1° série',
            'tipo' => 'Ensino Médio'
        ]);

        \App\Models\Serie::create([
            'descricao' => '2° série',
            'tipo' => 'Ensino Médio'
        ]);

        \App\Models\Serie::create([
            'descricao' => '3° série',
            'tipo' => 'Ensino Médio'
        ]);
    }

}
