<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TurmaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'turno' =>'required',
          'ano' =>'required',
          'descricao' => 'required',
          'id_serie' => 'required|min:1'
        ];
    }

  public function messages(){
    return [
      'turno.required'=>'O turno é obrigatório',
      'ano.required'=>'O ano é obrigatório',
      'descricao.required'=>'A descricao é obrigatório',
    ];
  }
}
