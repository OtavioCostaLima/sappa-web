<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
            <meta name="csrf-token" content="{{ csrf_token() }}">

      <script>
           window.Laravel = <?php echo json_encode([
               'csrfToken' => csrf_token(),
           ]); ?>
      </script>

</head>
<body>
  <div id="app">
    <md-toolbar>
  <h1 class="md-title">Sincronizar Alunos</h1>

    </md-toolbar>
    <nota></nota>
  </div>
</body>
<script src="js/app.js"></script>
</html>
