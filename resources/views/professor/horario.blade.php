@extends('templates.template')

@section('content')
<div class="container">
    <div class="row col s12 m12 l12">

        <ul class="collapsible col s12 m4 l4" data-collapsible="accordion">
            @foreach($professores as $professor)
            <li>

                <div class="collapsible-header"><span class="new badge">4</span><i
                        class="mdi mdi-account-circle"></i>
                    {{$professor->nome}}
                </div>

                <div class="collapsible-body"><p><b>Disciplinas</b></p>
                    @foreach($professor->horarios as $horario)
                    {{$horario->disciplina->descricao}}
                      ,  {{$horario->turma->descricao}}
                        <a href="{{route('horario.show',$horario->id)}}">deletar</a>
                        <br>
                    @endforeach
                    <a href="{{route('horario.edit',$professor->id)}}">adicionar disciplina</a>


                </div>
            </li>
            @endforeach
        </ul>
    </div>


</div>




<div class="fixed-action-btn">

    <a class="btn-floating btn-large waves-effect green waves-purple tooltipped" data-position="top" data-delay="50"
       data-tooltip="Cadastar" data-target="modal1"
       href="{{route('horario.create')}}"><i class="mdi mdi-plus"></i></a>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection