<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;



class Horario extends Model
{
    protected $fillable = ['id_turma', 'id_disciplina', 'id_professor'];

    public function professor(): BelongsTo
    {
        return $this->belongsTo(Professor::class, 'id_professor');
    }

    public function turma(): BelongsTo
    {
        return $this->belongsTo(Turma::class, 'id_turma');
    }

    public function disciplina(): BelongsTo
    {
        return $this->belongsTo(Disciplina::class, 'id_disciplina');
    }

}
