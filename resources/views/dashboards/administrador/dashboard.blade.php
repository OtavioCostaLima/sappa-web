@extends('templates.template')
@section('content')
  <main>
      <div class="row">
        <div class="col s6">
          <div style="padding: 35px;" align="center" class="card">
            <div class="row">
              <div class="left card-title">
                <b>Familia e Responsavel</b>
              </div>
            </div>

            <div class="row">
              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989968/seller_rcnkab.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Familia</h5></span>
                </div>
              </a>
              <div class="col s1">&nbsp;</div>
              <div class="col s1">&nbsp;</div>

              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989969/people_2_knqa3y.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Responsavel</h5></span>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div class="col s6">
          <div style="padding: 35px;" align="center" class="card">
            <div class="row">
              <div class="left card-title">
                <b>Professor e Aluno</b>
              </div>
            </div>
            <div class="row">
              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect  waves-purple">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989970/product_mdq6fq.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Professor</h5></span>
                </div>
              </a>

              <div class="col s1">&nbsp;</div>
              <div class="col s1">&nbsp;</div>

              <a href="{{url('dashboard/aluno')}}">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989970/stack_rwg2mz.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Aluno</h5></span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s6">
          <div style="padding: 35px;" align="center" class="card">
            <div class="row">
              <div class="left card-title">
                <b>Financeiro</b>
              </div>
            </div>

            <div class="row">
              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989969/brand_lldqpu.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Pagamentos</h5></span>
                </div>
              </a>

              <div class="col s1">&nbsp;</div>
              <div class="col s1">&nbsp;</div>

              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989969/brand_lldqpu.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Serviçoes</h5></span>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div class="col s6">
          <div style="padding: 35px;" align="center" class="card">
            <div class="row">
              <div class="left card-title">
                <b>Materiais</b>
              </div>
            </div>
            <div class="row">
              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989969/squares_dylwo9.png" class="responsive-img" /><br>
                  <span class="indigo-text text-lighten-1"><h5>Escolares</h5></span>
                </div>
              </a>
              <div class="col s1">&nbsp;</div>
              <div class="col s1">&nbsp;</div>

              <a href="#!">
                <div style="padding: 30px;" class="grey lighten-3 col s5 waves-effect">
                  <img src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989969/squares_dylwo9.png" class="responsive-img" /><br>
                  <span class="truncate indigo-text text-lighten-1"><h5>Limpeza</h5></span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </main>

@endsection
