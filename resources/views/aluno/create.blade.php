@extends('templates.template')

@section('content')
    <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width teal darken-3">
        <li class="tab col s3 m4 l4"><a href="#test-swipe-1" class="white-text">Aluno</a></li>
        <li class="tab col s3  m4 l4"><a href="#test-swipe-2" class="white-text">Familia</a></li>
        <li class="tab col s3  m4 l4"><a href="#test-swipe-3" class="white-text">Responsavel</a></li>
    </ul>

    <div class="container">

        @if(isset($aluno))
            {!! Form::model($aluno,['route'=>['aluno.update', $aluno->matricula],'class'=>'form','method'=>'put'])!!}
        @else
            {!!Form::open(['route'=>'aluno.store'])!!}
        @endif
<div class="card">
  <div class="card-title">
      <h2>Cadastrar Aluno</h2>
      <p>Primeiro Passo</p>
  </div>
  <div class="row" id="test-swipe-1">


      <!--formulário aluno-->
      <div class="input-field col s12 l8 m8">
          <i class="mdi mdi-36px mdi-account-circle prefix"></i>
          {!!Form::text('nome',null,['class'=>'validate','aria-describedby'=>'basic-addon1', 'id'=>'nome','required'])!!}
          <label for="nome">Nome Aluno</label>
      </div>

      <div class="input-field col s6 l4 m4">
          {!! Form::select('sexo', ['M' => 'Masculino', 'F' => 'Feminino'],null,['id'=>'sexo'])!!}
          <label for="sexo">Sexo</label>
      </div>

      <div class="input-field col s6 l4 m4">
          {!!Form::text('data_nascimento',null,['class'=>'datepicker','id'=>'data_nascimento'])!!}
          <label for="data_nascimento">Data Nascimento</label>
      </div>


      <div class="input-field col s6 l4 m4">
          {!!Form::text('naturalidade',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'naturalidade'])!!}
          <label for="naturalidade">Naturalidade</label>

      </div>

      <div class="input-field col s6 l4 m4">
          {!!Form::text('nacionalidade',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nacionalidade'])!!}
          <label for="nacionalidade">Nascionalidade</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="certidao" value=""
                 id="certidao" required>
          <label for="certidao">Certidão</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="termo" value=""
                 id="termo"
                 required>
          <label for="termo">Termo</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="folha" value=""
                 id="folha"
                 required>
          <label for="folha">Folha</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="livro" id="livro"
                 value=""
                 required>
          <label for="livro">Livro</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input name="data_emissao" type="date" class="datepicker" id="data_emissao" value="" required>
          <label for="data_emissao">Data Emissão</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="cartorio"
                 id="cartorio"
                 required>
          <label for="cartorio">Cartorio</label>
      </div>

      <div class="input-field col s12 l4 m4">
          <input name="email" type="text" class="validate" aria-describedby="basic-addon2" id="email"
                 required>
          <label for="email">Email</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input name="celular" type="text" class="validate" aria-describedby="basic-addon2"
                 id="celular" required>
          <label for="celular">Celular</label>
      </div>

      <div class="input-field col s6 l4 m4">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="cep" id="cep"
                 value="" required>
          <label for="cep">CEP</label>
      </div>

      <div class="input-field col s12 l6 m6">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="rua"
                 id="rua" value="" required>
          <label for="rua">Endereço</label>
      </div>

      <div class="input-field col s12 l6 m6">
          <input name="complemento" type="text" class="validate" id="complemento" required>
          <label for="complemento">Complemento</label>
      </div>

      <div class="input-field col s12 l6 m6">
          <input name="numero_residencia" type="text" class="validate" id="numero_residencia" required>
          <label for="numero_residencia">Número</label>
      </div>

      <div class="input-field col s12 l6 m6">
          <input name="bairro" type="text" class="validate" id="bairro" required>
          <label for="bairro">Bairro</label>
      </div>

      <div class="input-field col s12 l6 m6">
          <input name="cidade" type="text" class="validate" id="cidade" required>
          <label for="cidade">Cidade</label>
      </div>

      <div class="input-field col s12 l6 m6">
          {!!Form::select('uf',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'DistritoFederal','ES'=>'EspiritoSanto','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'MatoGrossodoSul','MT'=>'MatoGrosso','MG'=>'MinasGerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'RiodeJaneiro','RN'=>'RioGrandedoNorte','RS'=>'RioGrandedoSul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'SantaCatarina','SP'=>'SãoPaulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['class'=>'validate','id'=>'uf'])!!}
          <label for="uf">Estado</label>
      </div>

    </div>
</div>
    <!--fim formulário aluno-->






        <div class="row" id="test-swipe-2">
            <div class="card-panel">
                <h3>Dados da Mae</h3>
                <p>Segundo Passo</p>
            </div>
            <!-- formulário mae-->
            <div class="row">
                <div class="input-field col s12 l8 m8">
                    {!!Form::text('nome_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nome_mae'])!!}
                    <label for="nome_mae">Mãe</label>

                </div>

                <div class="input-field col s12 l4 m4">
                    {!!Form::select('estado_civil_mae',['solteira'=>'Solteira','casada'=>'Casada','separada'=>'Separada'],null,['class'=>'validate','id'=>'estado_civil_mae'])!!}
                    <label for="estado_civil_mae">Estado civil</label>
                </div>

                <div class="input-field col s6 l4 m4">
                    {!!Form::text('naturalidade_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'naturalidade_mae'])!!}
                    <label for="naturalidade_mae">Naturalidade</label>
                </div>


                <div class="input-field col s6 l4 m4">
                    {!!Form::text('nacionalidade_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nacionalidade_mae'])!!}
                    <label for="nacionalidade_mae">Nascionalidade</label>
                </div>

                <div class="input-field col s6 l4 m4">
                    {!!Form::text('cpf_mae',null,['class'=>'validate','id'=>'cpf_mae'])!!}
                    <label for="cpf_mae">CPF</label>
                </div>

                <div class="input-field col s6 l6 m6">
                    {!!Form::text('grau_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'grau_mae'])!!}
                    <label for="grau_mae">Grau</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::text('profissao_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'profissao_mae'])!!}
                    <label for="profissao_mae">Profissão</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::email('email_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'email_mae'])!!}
                    <label for="email_mae">Email</label>
                </div>

                <div class="input-field col s6 l3 m3">
                    <input name="celular_mae" type="text" class="validate" aria-describedby="basic-addon2"
                           id="celular_mae">
                    <label for="celular_mae">Celular</label>
                </div>

                <div class="input-field col s6 l3 m3">
                    <input type="text" class="validate" aria-describedby="basic-addon1" name="cep_mae" id="cep_mae"
                           value="">
                    <label for="cep_mae">CEP</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::text('rua_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'rua_mae'])!!}
                    <label for="rua__mae">Endereço</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::text('complemento_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'complemento_mae'])!!}
                    <label for="complemento_mae">Complemento</label>
                </div>

                <div class="input-field col s6 l4 m4">
                    {!!Form::text('numero_residencia_mae',null,['class'=>'formnumero_residencia_mae-control','aria-describedby'=>'basic-addon1','id'=>'numero_residencia_mae'])!!}
                    <label for="numero_residencia__mae">Número</label>
                </div>


                <div class="input-field col s12 l8 m8">
                    {!!Form::text('bairro_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'bairro_mae'])!!}
                    <label for="bairro_mae">Bairro</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::text('cidade_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'cidade_mae'])!!}
                    <label for="cidade_mae">Cidade</label>
                </div>

                <div class="input-field col s12 l6 m6">
                    {!!Form::select('uf_mae',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'DistritoFederal','ES'=>'EspiritoSanto','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'MatoGrossodoSul','MT'=>'MatoGrosso','MG'=>'MinasGerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'RiodeJaneiro','RN'=>'RioGrandedoNorte','RS'=>'RioGrandedoSul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'SantaCatarina','SP'=>'SãoPaulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['class'=>'validate','id'=>'uf_mae'])!!}
                    <label for="uf_mae">Estado</label>
                </div>
            </div>

            <h3>Dados da Pai</h3>
            <p>Segundo Passo</p>
            <div class="fa fa-edit">
                <hr>
                <div class="row">
                    <div class="input-field col s12 l8 m8">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="nome_pai" value=""
                               id="nome_pai">
                        <label for="nome_pai">Pai</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::select('estado_civil_pai',['solteira'=>'Solteira','casada'=>'Casada','separada'=>'Separada'],null,['class'=>'validate','id'=>'estado_civil_pai'])!!}
                        <label for="estado_civil_pai">Estado civil</label>
                    </div>


                    <div class="input-field col s12 l4 m4">
                        {!!Form::text('naturalidade_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'naturalidade_pai'])!!}
                        <label for="naturalidade_pai">Naturalidade</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::text('nacionalidade_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nacionalidade_pai'])!!}
                        <label for="nacionalidade_pai">Nascionalidade</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::text('cpf_pai',null,['class'=>'validate','id'=>'cpf_pai'])!!}
                        <label for="cpf_pai">CPF</label>
                    </div>
                    <div class="input-field col s12 l4 m4">
                        {!!Form::text('grau_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'grau_pai'])!!}
                        <label for="grau_pai">Grau</label>
                    </div>
                    <div class="input-field col s12 l8 m8">
                        {!!Form::text('profissao_pai',null,['class'=>'validate','Profissão','aria-describedby'=>'basic-addon1','id'=>'profissao_pai'])!!}
                        <label for="profissao_pai">Profissão</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        {!!Form::email('email_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'email_pai'])!!}
                        <label for="email_pai">Email</label>
                    </div>

                    <div class="input-field col s12 l3 m3">
                        <input name="celular_pai" type="text" class="validate" aria-describedby="basic-addon2"
                               id="celular_pai">
                        <label for="celular_pai">Celular</label>
                    </div>

                    <div class="input-field col s12 l3 m3">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="cep_pai"
                               id="cep_pai"
                               value="">
                        <label for="cep_pai">CEP</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        {!!Form::text('rua_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'rua_pai'])!!}
                        <label for="rua_pai">Endereço</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        <label for="complemento_pai">Complemento</label>
                        {!!Form::text('complemento_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'complemento_pai'])!!}
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::text('numero_residencia_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'numero_residencia_pai'])!!}
                        <label for="numero_residencia_pai">Número</label>
                    </div>

                    <div class="input-field col s12 l8 m8">
                        {!!Form::text('bairro_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'bairro_pai'])!!}
                        <label for="bairro_pai">Bairro</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        {!!Form::text('cidade_pai',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'cidade_pai'])!!}
                        <label for="cidade_pai">Cidade</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        {!!Form::select('uf_pai',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'DistritoFederal','ES'=>'EspiritoSanto','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'MatoGrossodoSul','MT'=>'MatoGrosso','MG'=>'MinasGerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'RiodeJaneiro','RN'=>'RioGrandedoNorte','RS'=>'RioGrandedoSul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'SantaCatarina','SP'=>'SãoPaulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['id'=>'uf_pai'])!!}
                        <label for="uf_pai">Estado</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="test-swipe-3">
            <div class="card-panel">
                <h3>Dados da Responsavel</h3>
                <p>Segundo Passo</p>
            </div>
            <!--RESPONSAVEL-->
            <div class="fa fa-edit">
                <div class="row">
                    <div class="input-field col s12 l8 m8">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="nome_contratante"
                               value="" id="nome_contratante">
                        <label for="nome_contratante">Responsável</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::select('civil',['solteira'=>'Solteira','casada'=>'Casada','separada'=>'Separada'],null,['class'=>'validate','id'=>'estado_civil_contratante'])!!}
                        <label for="estado_civil_contratante">Estado civil</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        <input type="text" class="validate" aria-describedby="basic-addon1"
                               name="naturalidade_contratante" id="naturalidade_contratante" value="">
                        <label for="naturalidade_contratante">Naturalidade</label>
                    </div>

                    <div class="input-field col s4 l4 m4">
                        <input type="text" class="validate" aria-describedby="basic-addon1"
                               name="nacionalidade_contratante" id="nacionalidade_contratante" value="">
                        <label for="nacionalidade_contratante">Nascionalidade</label>
                    </div>

                    <div class="input-field col s6 l4 m4">
                        {!!Form::text('cpf',null,['class'=>'validate','id'=>'cpf_contratante'])!!}
                        <label for="cpf_contratante">CPF</label>
                    </div>

                    <div class="input-field col s6 l4 m4">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="grau_contratante"
                               id="grau_contratante" value="">
                        <label for="grau_contratante">Grau</label>
                    </div>

                    <div class="input-field col s4 l8 m8">
                        {!!Form::text('profissao_contratante',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'profissao_contratante'])!!}
                        <label for="profissao_contratante">Profissão</label>
                    </div>

                    <div class="input-field col s4 l6 m6">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="email_contratante"
                               id="email_contratante" value="">
                        <label for="email_contratante">Email</label>
                    </div>

                    <div class="input-field col s12 l3 m3">
                        <input name="celular_contratante" type="text" class="validate"
                               aria-describedby="basic-addon2"
                               id="celular_contratante">
                        <label for="celular_contratante">Celular</label>
                    </div>

                    <div class="input-field col s4 l3 m3">
                        <input type="text" class="validate" aria-describedby="basic-addon1" name="cep_contratante"
                               id="cep_contratante" value="">
                        <label for="cep_contratante">CEP</label>
                    </div>

                    <div class="input-field col s4 l6 m6">
                        {!!Form::text('rua_contratante',null,['class'=>'validate','placeholder'=>'Endereço','aria-describedby'=>'basic-addon1','id'=>'rua_contratante'])!!}
                        <label for="rua_contratante">Endereço</label>
                    </div>

                    <div class="input-field col s12 l6 m6">
                        {!!Form::text('complemento_contratante',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'complemento_contratante'])!!}
                        <label for="complemento_contratante">Complemento</label>
                    </div>

                    <div class="input-field col s14 l4 m4">
                        {!!Form::text('numero_residencia_contratante',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'numero_residencia_contratante'])!!}
                        <label for="numero_residencia_contratante">Número</label>
                    </div>

                    <div class="input-field col s12 l8 m8">
                        {!!Form::text('bairro_contratante',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'bairro_contratante'])!!}
                        <label for="bairro_contratante">Bairro</label>
                    </div>

                    <div class="input-field col s12 l8 m8">
                        {!!Form::text('cidade_contratante',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'cidade_contratante'])!!}
                        <label for="cidade_contratante">Cidade</label>
                    </div>

                    <div class="input-field col s12 l4 m4">
                        {!!Form::select('uf_contratante',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'DistritoFederal','ES'=>'EspiritoSanto','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'MatoGrossodoSul','MT'=>'MatoGrosso','MG'=>'MinasGerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'RiodeJaneiro','RN'=>'RioGrandedoNorte','RS'=>'RioGrandedoSul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'SantaCatarina','SP'=>'SãoPaulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['class'=>'validate','id'=>'uf_contratante'])!!}
                        <label for="uf_contratante">Estado</label>
                    </div>
                </div>

            </div><!--FIM FORMULÁRIO RESPONSÁVEL-->
            </ul>
            <div class="input-field col s4 l4 m4 direct right">
                {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}</div>
            {!!Form::close()!!}
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
