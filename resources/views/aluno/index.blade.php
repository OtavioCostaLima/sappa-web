@extends('templates.template')

@section('content')
  <div class="container">
<div id="app">
<aluno-component></aluno-component>
</div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light darken-4 waves-purple" data-target="modal1"
           href="{{route('aluno.create')}}"><i class="mdi mdi-plus"></i></a>
    </div>
  </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>

@endsection
