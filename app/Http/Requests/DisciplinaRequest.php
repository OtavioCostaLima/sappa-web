<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DisciplinaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'descricao' => 'required|unique:disciplinas',
          'tipo'  => 'required'
        ];
    }

    public function messages(){
      return [
        'descricao.required'=>'A descricao é obrigatório',
        'descricao.unique'=>'Essa disciplina já existe!',
        'tipo.required'=>'O tipo é obrigatório',
      ];
    }
}
