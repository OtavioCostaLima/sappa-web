<?php

namespace App\Http\Controllers;

use App\Models\Bimestre;
use App\Models\Disciplina;
use App\Models\Professor;
use App\Models\Turma;
use Illuminate\Http\Request;
use App\Models\Nota;
use App\Models\TipoNota;

class NotaController extends Controller
{
    private $nota;
    private $bimestre;
    private $professor;
    private $turma;
    private $disciplina;
    private $tipo_nota;

    /**
     * NotaController constructor.
     * @param $nota
     * @param $bimestre
     * @param $professor
     * @param $turma
     */
    public function __construct(Nota $nota, Bimestre $bimestre, Professor $professor, Turma $turma, Disciplina $disciplina,TipoNota $tipo_nota)
    {
        $this->nota = $nota;
        $this->tipo_nota = $tipo_nota;
        $this->bimestre = $bimestre;
        $this->professor = $professor;
        $this->turma = $turma;
        $this->disciplina = $disciplina;


    }


    /**
     * NotaController constructor.
     * @param $nota
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professores = $this->professor->all('nome', 'id');
        $turmas = $this->turma->all(['id', 'descricao']);
        $disciplinas = $this->disciplina->all(['id', 'descricao'])->pluck('descricao', 'id');
        $bimestres = $this->bimestre->all()->pluck('descricao', 'id');
        $tipo_nota = $this->tipo_nota->all()->pluck('descricao', 'id');;
        return view('nota.index', compact('bimestres', 'professores', 'turmas', 'disciplinas', 'tipo_nota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
    $res =  $this->nota->create($data);
    return response($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
