<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

use App\Models\Horario;

class Turma extends Model
{

    protected $fillable = ['descricao', 'turno','ano','id_serie'];

 public function serie(): BelongsTo {
return $this->belongsTo(Serie::class,'id_serie');
 }

 public function alunos() {
 return $this->belongsToMany('App\Models\Aluno', 'aluno_turma', 'id_turma', 'matricula_aluno')->withPivot(['numero_chamada']);
 }

 public function horarios(): HasMany {
        return $this->hasMany(Horario::class,'id');
    }
}
