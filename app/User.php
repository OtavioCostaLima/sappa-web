<?php

namespace App;

use App\Models\Professor;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Mail;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function professor()    {
        return $this->hasOne(Professor::class);
    }

//professor é funcionário. apagar essa relação
    public function funcionario():HasOne
    {
        return $this->hasOne(Professor::class);
    }

    public function sendVerificationEmail()
    {

      Mail::send('mails.confirmation',
         ['token' => $this->token],
         function ($message) {
           $message->from('freestepotavio@gmail.com');
           $message->to($this->email);
           $message->subject('Por favor verifique seu email!');
           });

    }



}
