<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Rule::create([
          'nome' => 'PROFESSOR',
          'label' => 'Exerce o cargo de Professor na empresa!'

      ]);

      App\Rule::create([
          'nome' => 'RESPONSAVEL',
          'label' => 'Aquele que é Responsável legal do aluno!'

      ]);

      App\Rule::create([
          'nome' => 'FUNCIONARIO',
          'label' => 'Exerce um cargo em algum departamento da empresa!'

      ]);

      App\Rule::create([
          'nome' => 'ADM',
          'label' => 'Administrador do Sistema!'

      ]);
    }
}
