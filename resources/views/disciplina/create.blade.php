@extends('templates.template')
@section('content')
<div class="container">
    <div class="row">
        @if(isset($disciplina))
        {!! Form::model($disciplina,['route'=>['disciplinas.update', $disciplina->id],'class'=>'form','method'=>'put'])!!}
        @else
        {!!Form::open(['route'=>'disciplinas.store'])!!}
        @endif
        <div class="col s6 l6 m6">
            <label>Disciplina</label>
            {!! Form::text('descricao',null,['class'=>'validate','placeholder'=>'Nome da Disciplina'])!!}

            <label>Tipo</label>
            {!! Form::select('tipo', $tipos,null,['class'=>'input-field','required'=>""])!!}

            {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}
              {!!Form::close()!!}
        </div>
    </div>

</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
