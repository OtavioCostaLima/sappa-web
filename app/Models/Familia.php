<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Aluno;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Familia extends Model
{

    protected $fillable = [
        'nome_pai', 'nome_mae', 'estado_civil_mae', 'estado_civil_pai', 'nacionalidade_mae', 'nacionalidade_pai', 'cpf_mae', 'cpf_pai', 'grau_mae'
        , 'grau_pai', 'naturalidade_mae', 'naturalidade_pai', 'profissao_mae', 'profissao_pai', 'rua_mae'
        , 'rua_pai', 'numero_residencia_mae', 'numero_residencia_pai', 'bairro_mae', 'bairro_pai', 'cep_mae'
        , 'cep_pai', 'celular_mae', 'celular_pai', 'cidade_mae', 'cidade_pai', 'uf_mae'
        , 'uf_pai', 'email_mae', 'email_pai'];

        public $rules = [
              'nome_pai' => 'required',
              'nome_mae'=> 'required',
              'estado_civil_mae'=> 'required',
              'estado_civil_pai'=> 'required',
              'nacionalidade_mae'=> 'required',
              'nacionalidade_pai' => 'required',
              'cpf_mae' => 'required',
              'cpf_pai' => 'required',
              'grau_mae' => 'required',
               'grau_pai' => 'required',
              'naturalidade_mae' => 'required',
              'naturalidade_pai' => 'required',
              'profissao_mae' => 'required',
              'profissao_pai' => 'required',
              'rua_mae' => 'required',
              'rua_pai' => 'required',
              'numero_residencia_mae' => 'required',
              'numero_residencia_pai' => 'required',
              'bairro_mae', 'bairro_pai' => 'required',
              'cep_mae' => 'required',
              'cep_pai' => 'required',
              'celular_mae' => 'required',
              'celular_pai' => 'required',
              'cidade_mae' => 'required',
              'cidade_pai' => 'required',
              'uf_mae' => 'required',
              'uf_pai' => 'required',
              'email_mae' => 'required',
              'email_pai' => 'required'
        ];


    public function alunos(): HasMany
    {
        return $this->hasMany(Aluno::class);
    }

}
