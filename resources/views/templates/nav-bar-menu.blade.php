<!-- Dropdown Structure -->

<nav>
    <div class="nav-wrapper teal darken-3">
        <a href="/home" class="brand-logo">Início</a>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi mdi-menu"></i></a>
        <ul class="right hide-on-med-and-down">
            <li><a class="mdi mdi-lead-pencil tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Disciplina" href="{{url(route("disciplinas.index"))}}"></a></li>
            <li><a class="mdi mdi-face-profile tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Professor" href="{{url(route("professor.index"))}}"></a></li>
            <li><a class="mdi mdi-note tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Notas" href="{{url(route("nota.index"))}}"></a></li>
            <li><a class="mdi mdi-face tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Aluno" href="{{url(route("aluno.index"))}}"></a></li>
            <li><a class="mdi mdi-alphabetical tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Turma" href="{{url(route("turma.index"))}}"></a></li>
            <li><a class="mdi mdi-calendar tooltipped" data-position="botton" data-delay="50"
                   data-tooltip="Horario" href="{{url(route("horario.index"))}}"></a></li>
            @if(!Auth::guest())
                <ul class="right hide-on-med-and-down">
                    <!-- Dropdown Trigger -->

                    <li>
                        <a class="dropdown-button" href="#!" data-activates="dropdown"
                           data-beloworigin="true">{{Auth::user()->name }}
                            <i class="mdi-navigation-arrow-drop-down right"></i>
                        </a>
                    </li>
                </ul>

                <ul id="dropdown" class="dropdown-content collection">
                    <li class="collection-item avatar">
                        <img src="{{url('icones/man.png')}}" class="circle ">
                        <span class="title">{{Auth::user()->name }}</span>
                        <p>First Line</p>

                        <a class="secondary-content" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-exit-to-app"></i>Sair
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            @endif
        </ul>

        <ul class="side-nav" id="slide-out">
            <div class="collection">
                @if(!Auth::guest())
                    <li class="collection-item avatar teal darken-3">
                        <img src="{{url('icones/man.png')}}" alt="" class="circle">
                        <p class="title"> {{Auth::user()->name}}</p>
                        <a class="secondary-content white-text" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-exit-to-app white-text"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
                <li><a class="mdi mdi-lead-pencil tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Disciplina" href="{{url(route("disciplinas.index"))}}">Disciplina</a></li>
                <li><a class="mdi mdi-face-profile tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Professor" href="{{url(route("professor.index"))}}">Professor</a></li>
                <li><a class="mdi mdi-note tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Notas" href="{{url(route("nota.index"))}}">Notas</a></li>
                <li><a class="mdi mdi-face tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Aluno" href="{{url(route("aluno.index"))}}">Aluno</a></li>
                <li><a class="mdi mdi-alphabetical tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Turma" href="{{url(route("turma.index"))}}">Turma</a></li>
                <li><a class="mdi mdi-calendar tooltipped" data-position="botton" data-delay="50"
                       data-tooltip="Horario" href="{{url(route("horario.index"))}}">Horario</a></li>
            </div>
        </ul>
    </div>
</nav>


<style type="text/css">
    .brand-logo {
        font-weight: 900;
    }

    .dropdown-content {
        background-color: #FFFFFF;
        margin: 0;
        display: none;
        min-width: 300px; /* Changed this to accomodate content width */
        max-height: auto;
        margin-left: -1px; /* Add this to keep dropdown in line with edge of navbar */
        overflow: hidden; /* Changed this from overflow-y:auto; to overflow:hidden; */
        opacity: 0;
        position: absolute;
        white-space: nowrap;
        z-index: 1;
        will-change: width, height;
    }
</style>
