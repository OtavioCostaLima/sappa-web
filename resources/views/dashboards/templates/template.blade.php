<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{url("materialize/css/materialize.css")}}" rel="stylesheet" type="text/css"/>
  <link href="{{url("material-design-icones/css/materialdesignicons.css")}}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/css/style.css" type="text/css">
  <title>{{isset($title)?$title:"Sistema Acadêmico"}}</title>
</head>
<style media="screen">
  
</style>
  <body>
    @include('dashboards.templates.header')
    @include('dashboards.templates.main')
    @include('dashboards.templates.footer')
  </body>

  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
  <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>


</html>
