require('./bootstrap');

Vue.component('aluno-component', require('./components/AlunoComponent.vue'));
Vue.component('aluno', require('./components/Aluno.vue'));
Vue.component('aluno-turma', require('./components/AlunoTurma.vue'));
Vue.component('nota', require('./components/Nota.vue'));
Vue.component('responsavel', require('./components/Responsavel.vue'));
Vue.component('app', require('./components/App.vue'));
Vue.component('lancar-notas', require('./components/LancarNotas.vue'));
import Vuetify from 'vuetify'

Vue.use(Vuetify)

require('vue-material/dist/vue-material.css');
require('mdi/css/materialdesignicons.css');

var VueMaterial = require('vue-material');

Vue.use(VueMaterial);

Vue.material.registerTheme({
  default: {
    primary: 'teal',
    accent: 'red'
  },
  green: {
    primary: 'green',
    accent: 'pink'
  },
  orange: {
    primary: 'orange',
    accent: 'green'
  },
})

const app = new Vue({
    el: '#app'

});
