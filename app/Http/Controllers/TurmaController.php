<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TurmaRequest;
use App\Models\Turma;
use App\Models\Serie;

class TurmaController extends Controller
{

    private $turma;

    function __construct(Turma $turma)
    {
        $this->turma = $turma;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Index Turma";
        $turnos = ['Manhã' => 'Manhã', 'Tarde' => 'Tarde', 'Noite' => 'Noite'];
        $turmas = $this->turma->all();
        return view('turma.index', compact('title', 'turmas','turnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Serie $serie)
    {
         $series = $serie->all(['id', 'descricao'])->pluck('descricao','id');
        $title = "Cadastro Turma";
        $turnos = ['Manhã' => 'Manhã', 'Tarde' => 'Tarde', 'Noite' => 'Noite'];
        return view('turma.create', compact('title', 'turnos','series'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TurmaRequest $request)
    {
      $dados = $request->all();
      $insert = $this->turma->create($dados);

        if ($insert) {
            return redirect()->route('turma.index');
        } else {
            return redirect()->route('turma.store');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dis = $this->turma->find($id);
        $delete = $dis->delete();
        if ($delete) {
            return redirect()->route('turma.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $series = Serie::all(['id', 'descricao'])->pluck('descricao','id');
        $turma = $this->turma->find($id);
        $title = "Editar: {{$turma->descricao}}";
        $turnos = ['Manhã' => 'Manhã', 'Tarde' => 'Tarde', 'Noite' => 'Noite'];
        return view('turma.create', compact('turma', 'title', 'turnos','series'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TurmaRequest $request, $id)
    {
        $dados = request()->all();
        $tur = $this->turma->find($id);
        if ($tur->update($dados)) {
            return redirect()->route('turma.index');
        } else {
            return redirect('turma.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    Retorna um Json com todas as turmas existentes!
    */
    public function getTurmas()
    {
      $turmas = $this->turma->all();
      return response()->json($turmas);
    }

}
