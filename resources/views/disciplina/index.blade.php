@extends('templates.template')

@section('content')

<div class="alert">
@if(isset($errors) && count($errors)>0)
  @foreach ($errors->all() as $error)
{{$error}}
  @endforeach
@endif

</div>
<div class="container">
  <div class="card">
      <table class="highlight centered bordered">
        <thead>
            <tr>
                <th>Disciplina</th>
                <th>Tipo</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @if(isset($disciplinas))
                @foreach($disciplinas as $disciplin)
                <td> {{$disciplin->descricao}}</td>
                <td> {{$disciplin->tipo}}</td>
                <td>
                    <a class="mdi mdi-account-edit mdi-24px" href="{{route('disciplinas.edit',$disciplin->id)}}"></a>
                    <a class="mdi mdi-delete mdi-24px" href="{{route('disciplinas.show',$disciplin->id)}}"></a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
  </div>


<div class="paginate center">
{!!  $disciplinas->links('vendor.pagination.materiallize')!!}
</div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light red" data-target="modal1" href="{{route('disciplinas.create')}}"><i class="mdi mdi-plus"></i></a>
    </div>
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Disciplina</h4>
            <div class="row">
                @if(isset($disciplina))
                {!! Form::model($disciplina,['route'=>['disciplinas.update', $disciplina->id],'class'=>'form','method'=>'put'])!!}
                @else
                @endif
                {!!Form::open(['route'=>'disciplinas.store'])!!}

                <div class="col s12 l12 m12">
                    <label>Disciplina</label>
                    {!! Form::text('descricao',null,['class'=>'validate','placeholder'=>'Nome da Disciplina'])!!}

                    <label>Tipo</label>
                    {!! Form::select('tipo', $tipos,null,['class'=>'input-field','required'=>""])!!}

                    {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>

   </div>


    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light darken-3 waves-purple" data-target="modal1" href="{{route('disciplinas.create')}}"><i class="mdi mdi-plus"></i></a>
    </div>
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Disciplina</h4>
            <div class="row">
                @if(isset($disciplina))
                {!! Form::model($disciplina,['route'=>['disciplinas.update', $disciplina->id],'class'=>'form','method'=>'put'])!!}
                @else
                @endif
                {!!Form::open(['route'=>'disciplinas.store'])!!}

                <div class="col s12 l12 m12">
                    <label>Disciplina</label>
                    {!! Form::text('descricao',null,['class'=>'validate','placeholder'=>'Nome da Disciplina'])!!}

                    <label>Tipo</label>
                    {!! Form::select('tipo', $tipos,null,['class'=>'input-field','required'=>""])!!}

                    {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
