<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Controle Impacto') }}</title>

<!-- Styles
     <link href="{{ url('mate/app.css') }}" rel="stylesheet">
    -->
    <link href="{{url("materialize/css/materialize.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{url("material-design-icones/css/materialdesignicons.css")}}" rel="stylesheet" type="text/css">


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

<div class="container">

    <div class="card bordered">
        <div class="card-header center teal darken-3">
            <i class="mdi mdi-account mdi-48px white-text"></i>
        </div>

        <div class="card-content">
            <form role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="input-field col s8 m6 l6">
                  <select class="validate" name="tipo_user">
                      <option value="professor">Professor</option>
                      <option value="aluno">Aluno</option>
                      <option value="funcionario">Funcionário</option>
                      <option value="responsavel">Responsável</option>
                  </select>
                  <label for="tipo_user">Entrar como</label>
                </div>

                <div class="input-field col s8 m6 l6 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="validate" name="email"
                           value="{{ old('email') }}" required autofocus>
                    <label for="email">Endereço de Email</label>
                    @if ($errors->has('email'))
                        <span class="help-block red-text text-darken-2">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="input-field col s8 m6 l6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="validate" name="password" required>
                    <label for="password">Password</label>
                    @if ($errors->has('password'))
                        <span class="help-block red-text text-darken-2">
                                        <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div>
                <br>


                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember"> Lembrar-me</label>


                <button type="submit" class="btn teal right waves-effect waves-light white-text">
                    Entrar
                </button>

        </div>
        </form>
        <div class="card-action">
            <div class="row">
                <div class="col s6 m8 l8 text-p">
                    <a href="{{ route('password.request') }}">Esqueceu sua senha?</a>
                </div>
                <div class="col s6 m4 l4 right-align text-p">
                    <a href="{{ route('register') }}">Resgistrar</a>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<!-- Scripts -->

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
<script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>

<script>
    $(".button-collapse").sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
    });
</script>
</body>
<style type="text/css">
body{
  background-image: url('icones/imagem.jpg');
  background-repeat: no-repeat;
}

.card {
  margin: 0% auto;
  margin-top: 5%;
  max-width:400px;
}

    .brand-logo {
        font-weight: 900;
    }

    .dropdown-content {
        background-color: #FFFFFF;
        margin: 0;
        display: none;
        min-width: 300px; /* Changed this to accomodate content width */
        max-height: auto;
        margin-left: -1px; /* Add this to keep dropdown in line with edge of navbar */
        overflow: hidden; /* Changed this from overflow-y:auto; to overflow:hidden; */
        opacity: 0;
        position: absolute;
        white-space: nowrap;
        z-index: 1;
        will-change: width, height;
    }
</style>
</html>
