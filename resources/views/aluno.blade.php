<!DOCTYPE html>
<html>
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
</script>
    <meta charset="utf-8">
    <title>Aluno</title>
  </head>
  <body>
<div id="app">
  <aluno></aluno>
</div>
  </body>

  <script src="js/app.js">
   </script>

</html>
