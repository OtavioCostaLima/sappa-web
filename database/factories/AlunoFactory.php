<?php
$factory->define(App\Models\Aluno::class, function (Faker\Generator $faker) {
    static $password;

    return [
      'nome'=>$faker->name,
      'data_nascimento' =>'required',
      'sexo' =>'F',
      'bairro'=>$faker->region,
      'celular'=>$faker->mobileNumber,
      'cep'=>$faker->postcode,
      'uf'=>$faker->stateAbbr,
      'cidade'=>$faker->city,
      'cor'=>'required',
      'data_emissao'=>$faker->dateTime(),
      'email'=>$faker->unique()->safeEmail,
      'folha'=>'required',
      'livro'=>'required',
      'nacionalidade'=>'required',
      'numero_residencia'=>$faker->buildingNumber,
      'registro_nascimento'=>$faker->dateTime(),
      'rua'=>$faker->streetName,
      'status'=>'ativo',
      'telefone'=>$faker->mobileNumber
      ];
});
