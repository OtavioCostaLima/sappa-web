<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $this->call(AlunoSeeder::class);
      $this->call(UserTableSeeder::class);
      $this->call(SerieSeeder::class);
    //  $this->call(TurmaSeeder::class);
      $this->call(BimestreSeeder::class);
      $this->call(TipoNotaSeeder::class);
      //$this->call(ProfessorSeeder::class);

        //$this->call(FamiliaSeeder::class);
        //$this->call(ContratanteSeeder::class);

        //$this->call(UsersTableSeeder::class);
        //$this->call(RoleTableSeeder::class);
    }

}
