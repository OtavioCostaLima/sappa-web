<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashBoardController extends Controller
{
  public function dashboardAdministrador(Request $request){
    if($request->page="aluno"){
      return view('dashboards.administrador.aluno');
    }
  }
}
