<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Nota;
use App\Models\Familia;

/**
Classe aluno onde se tem os atributos!
*/
class Aluno extends Model {
protected $fillable =  ['matricula','familia_id','nome','data_nascimento','sexo','bairro','celular','cep','uf','cidade','cor','data_emissao','email','folha','livro','nacionalidade','numero_residencia','registro_nascimento','rua','status','telefone'];

    protected $primaryKey = 'matricula';

    public function notas(): HasMany {
        return $this->hasMany(Nota::class, 'matricula');
    }

    public function familia(): BelongsTo {
        return $this->belongsTo(Familia::class);
    }

    public function turmas() {
 return $this->belongsToMany('App\Models\Turma', 'aluno_turma', 'matricula_aluno', 'id_turma')->withPivot(['numero_chamada']);
    }

    public function contratante() {
        return $this->belongsToMany('App\Models\Contratante', 'contratantes', 'matricula_aluno', 'id_contratante')->withPivot(['data_contrato', 'data_renovacao']);
    }

}
