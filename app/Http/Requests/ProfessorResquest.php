<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfessorResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nome' => 'required',                     'cpf'  => 'required',
          'bairro'  => 'required',                  'naturalidade'  => 'required',
          'nascionalidade'  => 'required',          'sexo'  => 'required',
          'email' => 'required|email|max:255|unique:users|unique:professors',  
            'ctps' => 'required',
          'data_emissao' => 'required',             'profissao' => 'required',
          'data_nascimento' => 'required',          'estado_civil'  => 'required',
          'numero_residencia' => 'required',        'orgao_emissor' => 'required',
          'pis_pasep' => 'required',                'pos_graduacao' => 'required',
          'cidade' => 'required',                   'etinia' => 'required',
          'grau_instrucao' => 'required',           'rg' => 'required',
          'celular' => 'required',                  'endereco' => 'required',
      //    'secao' => 'required',                    'serie' => 'required',
        //  'situacao' => 'required',
          // 'titulo_eleitor' => 'required',
          'estado'   => 'required',                 'complemento',
          'cep' => 'required'
          ];
    }

    public function messages(){
      return [
        'nome.required'=>'O nome é obrigatório',
        'email.unique'=>'Esse email já existe!',
        'cpf.required'=>'O cpf é obrigatório',
      ];
    }
}
