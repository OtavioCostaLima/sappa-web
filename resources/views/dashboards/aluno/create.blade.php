@extends('dashboards.templates.template')
@section('conteudo')
<style media="screen">
  body {
    background-color: rgb(207, 221, 204);
  }
</style>
    <div class="container">

        @if(isset($aluno))
            {!! Form::model($aluno,['route'=>['aluno.update', $aluno->matricula],'class'=>'form','method'=>'put'])!!}
        @else
            {!!Form::open(['route'=>'aluno.store'])!!}
        @endif

<div class="card-panel">
      <h4>Cadastrar Aluno</h4>
      <p>Primeiro Passo</p>
<div class="row">
  <!--formulário aluno-->
  <div class="input-field col s12 l8 m8">
      <i class="mdi mdi-36px mdi-account-circle prefix"></i>
      {!!Form::text('nome',null,['class'=>'validate','aria-describedby'=>'basic-addon1', 'id'=>'nome','required'])!!}
      <label for="nome">Nome Aluno</label>
  </div>

  <div class="input-field col s6 l4 m4">
      {!! Form::select('sexo', ['M' => 'Masculino', 'F' => 'Feminino'],null,['id'=>'sexo'])!!}
      <label for="sexo">Sexo</label>
  </div>

  <div class="input-field col s6 l4 m4">
      {!!Form::text('data_nascimento',null,['class'=>'datepicker','id'=>'data_nascimento'])!!}
      <label for="data_nascimento">Data Nascimento</label>
  </div>


  <div class="input-field col s6 l4 m4">
      {!!Form::text('naturalidade',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'naturalidade'])!!}
      <label for="naturalidade">Naturalidade</label>

  </div>

  <div class="input-field col s6 l4 m4">
      {!!Form::text('nacionalidade',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nacionalidade'])!!}
      <label for="nacionalidade">Nascionalidade</label>
  </div>

  <div class="input-field col s6 l4 m4">
      <input type="text" class="validate" aria-describedby="basic-addon1" name="certidao" value=""
             id="certidao" required>
      <label for="certidao">Certidão</label>
  </div>

  <div class="input-field col s12 l4 m4">
      <input name="email" type="text" class="validate" aria-describedby="basic-addon2" id="email"
             required>
      <label for="email">Email</label>
  </div>

  <div class="input-field col s6 l4 m4">
      <input type="text" class="validate" aria-describedby="basic-addon1" name="cep" id="cep"
             value="" required>
      <label for="cep">CEP</label>
  </div>

  <div class="input-field col s12 l6 m6">
      <input type="text" class="validate" aria-describedby="basic-addon1" name="rua"
             id="rua" value="" required>
      <label for="rua">Endereço</label>
  </div>

  <div class="input-field col s12 l6 m6">
      <input name="complemento" type="text" class="validate" id="complemento" required>
      <label for="complemento">Complemento</label>
  </div>

  <div class="input-field col s12 l6 m6">
      <input name="numero_residencia" type="text" class="validate" id="numero_residencia" required>
      <label for="numero_residencia">Número</label>
  </div>

  <div class="input-field col s12 l6 m6">
      <input name="bairro" type="text" class="validate" id="bairro" required>
      <label for="bairro">Bairro</label>
  </div>

  <div class="input-field col s12 l6 m6">
      <input name="cidade" type="text" class="validate" id="cidade" required>
      <label for="cidade">Cidade</label>
  </div>

  <div class="input-field col s12 l6 m6">
      {!!Form::select('uf',['AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'DistritoFederal','ES'=>'EspiritoSanto','GO'=>'Goiás','MA'=>'Maranhão','MS'=>'MatoGrossodoSul','MT'=>'MatoGrosso','MG'=>'MinasGerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'RiodeJaneiro','RN'=>'RioGrandedoNorte','RS'=>'RioGrandedoSul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'SantaCatarina','SP'=>'SãoPaulo','SE'=>'Sergipe','TO'=>'Tocantins'],null,['class'=>'validate','id'=>'uf'])!!}
      <label for="uf">Estado</label>
  </div>

  <!--fim formulário aluno-->



<!--RESPONSAVEL-->
<div class="fa fa-edit">
<div class="col s12 m12 l12 ">
  <h4>Dados do Responsavel</h4>
</div>

        <div class="input-field col s12 l8 m8">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="nome_contratante"
                 value="" id="nome_contratante">
          <label for="nome_contratante">Responsável</label>
      </div>

      <div class="input-field col s6 l4 m4">
          {!!Form::text('cpf',null,['class'=>'validate','id'=>'cpf_contratante'])!!}
          <label for="cpf_contratante">CPF</label>
      </div>

      <div class="input-field col s4 l6 m6">
          <input type="text" class="validate" aria-describedby="basic-addon1" name="email_contratante"
                 id="email_contratante" value="">
          <label for="email_contratante">Email</label>
      </div>
  <!--FIM FORMULÁRIO RESPONSÁVEL-->
</div>
  <!-- formulário mae-->

      <div class="input-field col s12 l8 m8">
          {!!Form::text('nome_mae',null,['class'=>'validate','aria-describedby'=>'basic-addon1','id'=>'nome_mae'])!!}
          <label for="nome_mae">Mãe</label>
      </div>

          <div class="input-field col s12 l8 m8">
              <input type="text" class="validate" aria-describedby="basic-addon1" name="nome_pai" value=""
                     id="nome_pai">
              <label for="nome_pai">Pai</label>
          </div>

<!--Contato-->

<div class="col s12 m12 l12 ">
  <h4>Contato</h4>
</div>

<div class="input-field col s6 l4 m4">
    <input name="celular" type="text" class="validate" aria-describedby="basic-addon2"
           id="celular" required>
    <label for="celular">Celular</label>
</div>

        <div class="col s12 l12 m12 ">
          <div class="input-field right">
              {!!Form::submit('Enviar',['class'=>'btn btn-primary'])!!}
          </div>
          {!!Form::close()!!}
        </div>

          </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
