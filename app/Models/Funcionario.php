<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
  protected $fillable = [
    'nome','cpf','rg','orgao_expeditor',
    'nascionalidade','endereco','numero',
    'bairro','cep','cidade','telefone',
    'celular','email','profissao',
    'endereco_profissional','numero_endereco_profissional',
    'telefone_comercial'
  ];

  public $rules = [
    'nome' => 'required',
    'cpf' => 'required',
    'rg' => 'required',
    'orgao_expeditor' => 'required',
    'nascionalidade' => 'required',
    'endereco' => 'required',
    'numero' => 'required',
    'bairro' => 'required',
    'cep' => 'required',
    'cidade' => 'required',
    'telefone' => 'required',
    'celular' => 'required',
    'email' => 'required',
    'profissao' => 'required',
    'endereco_profissional' => 'required',
    'numero_endereco_profissional' => 'required',
    'telefone_comercial' => 'required'
  ];
}
