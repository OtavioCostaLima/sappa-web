<?php

namespace App\Http\Controllers;

use App\Models\Contratante;
use Illuminate\Http\Request;
use App\Models\Aluno;
use App\Models\Familia;
use App\Models\AlunoTurma;
use App\Http\Requests\AlunoRequest;

class AlunoController extends Controller
{

    private $aluno;
    private $a;

    function __construct(Aluno $aluno, Familia $fam)
    {
        $this->aluno = $aluno;
        $this->a = $fam;
    }

    public function index()
    {
        $alunos = $this->aluno->all();
        $title = 'Pagina Aluno';
        return view('aluno.index', compact('alunos', 'title'));
    }

    public function create()
    {
        $title = 'Cadastro de Alunos';
        return view('aluno.create', compact('title'));
    }

    public function edit($id)
    {
        $tipos = ['Reprova' => 'Reprova', 'Não Reprova' => 'Não Reprova'];
        $aluno = $this->aluno->find($id);
        $title = "Editar: {{$aluno->descricao}}";
        return redirect()->view('aluno.index', compact('aluno', 'title', 'tipos'));
    }

    public function update(AlunoRequest $request, $id)
    {
        $dados = request()->except('_token', '_method');
        $disc = $this->aluno->find($id);
        if ($disc->update($dados)) {
            return redirect('aluno');
        } else {
            return redirect('aluno.edit');
        }
    }

    public function store(AlunoRequest $request)
    {
        $dados = $request->except('_token');
        $familina = Familia::create($dados);
        $aluno = $this->aluno->create($dados);
        $insert = $aluno->familia()->associate($familina);
        $insert->save();
        if ($insert) {
            return redirect()->route('aluno.index');
        } else {
            return redirect()->route('aluno.store');
        }
    }

    public function a(AlunoRequest $request)
    {
      $alunos = $this->aluno->all();
       $alunos->toJson( );
      var_dump($alunos);

    }

    public function aniversariantes()
    {
        $alunos = $this->aluno->all();
        $this->aluno->whereDate('data_nascimento', '==', date('m-d'));
        return view('aluno.turma', compact('alunos'));
    }


    /**
     Retorna alunos não vinculados a uma turma
     Rota: alunos/vinculo=false
    */
    public function getAlunosVinculados()
    {
        return response()->json($this->aluno->all());
    }

/**
 Retorna alunos não vinculados a uma turma
 Rota: alunos/vinculo=false
*/
public function getAlunosNaoVinculados(){
  $alunos =  $this->aluno->whereNotIn('matricula', function($q){
    $q->select('matricula_aluno')->from('aluno_turma');
})->get();

  return $alunos->toJson( );

}

public function getAlunos(AlunoRequest $request){
  $nome = $request->shearch;
try {
  $alunos =  $this->aluno->where('nome','LIKE',"%$nome%")->paginate(10);
  return response()->json($alunos);
} catch (Exception $e) {
return $e.getMessage();
}

}

}
