<?php

namespace App\Http\Controllers;

use App\Models\TipoNota;
use Illuminate\Http\Request;
use App\Models\Professor;
use App\Models\Turma;
use App\Models\Disciplina;
use App\Models\Horario;
use App\Http\Requests\HorarioRequest;

class HorarioController extends Controller
{

    private $professor;
    private $turma;
    private $disciplina;
    private $horario;


    /**
     * HorarioController constructor.
     * @param $professor
     * @param $turma
     * @param $disciplina
     */
    public function __construct(Professor $professor, Turma $turma, Disciplina $disciplina, Horario $horario)
    {
        $this->professor = $professor;
        $this->turma = $turma;
        $this->disciplina = $disciplina;
        $this->horario = $horario;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professores = $this->professor->all();
        $turmas = $this->turma->all(['id', 'descricao'])->pluck('descricao', 'id');
        $disciplinas = $this->disciplina->all(['id', 'descricao'])->pluck('descricao', 'id');
        return view('professor.horario', compact('professores', 'turmas', 'disciplinas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $horarios = $this->horario->all();
        $professores = $this->professor->all('id', 'nome')->pluck('nome', 'id');
        $turmas = $this->turma->all(['id', 'descricao'])->pluck('descricao', 'id');
        $disciplinas = $this->disciplina->all(['id', 'descricao'])->pluck('descricao', 'id');
        return view('horario.create', compact('professores', 'turmas', 'disciplinas', 'horarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HorarioRequest $request)
    {
        $dados = $request->except('_token');
        $insert = $this->horario->create($dados);
        if ($insert) {
            return redirect()->route('horario.index');
        } else {
            return redirect()->route('professor.store');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $horario = $this->horario->find($id);
        $delete = $horario->delete();
        if ($delete) {
            return redirect()->route('horario.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professor = $this->professor->find($id);
        $turmas = $this->turma->all(['id', 'descricao'])->pluck('descricao', 'id');
        $disciplinas = DisciplinaController::getDisciplinasHorario($professor->id)->pluck('descricao', 'id');;
        return view('horario.edit', compact('turmas', 'professor', 'disciplinas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HorarioRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

public function getHorario($idTurma,$idProfessor,$idDisciplina){
$resp = $this->horario->select('id as id_horario')->where([['id_turma', '=', $idTurma],['id_professor', '=', $idProfessor],['id_disciplina', '=', $idDisciplina],])->get();
return $resp;
}

}
