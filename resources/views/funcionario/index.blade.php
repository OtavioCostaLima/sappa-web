@extends('templates.template')
@section('content')

<table class="table table-hover table-bordered text-center">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Setor</th>
            <th>Ações</th>
        </tr>
    </thead>

    <tbody>
        @foreach($funcionarios as $funcionario)
        <tr>
            <th>{{$funcionario->nome}}</th>
            <th>{{$funcionario->profissao}}</th>
            <th>Ações</th>
        </tr>
        @endforeach
    </tbody>

</table>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
    <script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>
@endsection
