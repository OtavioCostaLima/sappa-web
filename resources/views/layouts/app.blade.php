<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Controle Impacto') }}</title>

<!-- Styles
     <link href="{{ url('mate/app.css') }}" rel="stylesheet">
    -->
    <link href="{{url("materialize/css/materialize.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{url("material-design-icones/css/materialdesignicons.css")}}" rel="stylesheet" type="text/css">


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
  @if(!Auth::guest())
    @if (!is_null(Auth::user()->professor))
      {{var_dump(Auth::user()->professor->nome)}}
    @endif


@endif
<div id="app">
    <nav>
        <div class="nav-wrapper teal darken-3">
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Inicio
            </a>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi mdi-menu"></i></a>

            <ul class="right hide-on-med-and-down">

                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Resgistrar</a></li>
                @else
                    <ul class="right hide-on-med-and-down">
                        <!-- Dropdown Trigger -->
                        <li><a class="mdi mdi-lead-pencil tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Disciplina" href="{{url(route("disciplinas.index"))}}"></a></li>
                        <li><a class="mdi mdi-face-profile tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Professor" href="{{url(route("professor.index"))}}"></a></li>
                        <li><a class="mdi mdi-note tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Notas" href="{{url(route("nota.index"))}}"></a></li>
                        <li><a class="mdi mdi-face tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Aluno" href="{{url(route("aluno.index"))}}"></a></li>
                        <li><a class="mdi mdi-alphabetical tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Turma" href="{{url(route("turma.index"))}}"></a></li>
                        <li><a class="mdi mdi-calendar tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Horario" href="{{url(route("horario.index"))}}"></a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown"
                               data-beloworigin="true">{{Auth::user()->name }}
                                <i class="mdi-navigation-arrow-drop-down right"></i>
                            </a>
                        </li>
                    </ul>

                    <ul id="dropdown" class="dropdown-content collection">
                        <li class="collection-item avatar">
                            <img src="{{url('icones/man.png')}}" class="circle ">
                            <span class="title">

                            @if (!is_null(Auth::user()->professor))
                            é
                            @endif</span>
                            <p>First Line</p>

                            <a class="secondary-content" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="mdi mdi-exit-to-app"></i>Sair
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                @endif
            </ul>

            <ul class="side-nav" id="slide-out">
                <div class="collection">

                    @if(Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Resgistrar</a></li>
                    @else
                        <li class="collection-item avatar teal darken-3">
                            <img src="{{url('icones/man.png')}}" class="circle ">
                            <p class="title"> {{Auth::user()->name}}</p>
                            <a class="secondary-content white-text" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="mdi mdi-exit-to-app white-text"></i>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li><a class="mdi mdi-lead-pencil tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Disciplina" href="{{url(route("disciplinas.index"))}}">Disciplina</a></li>
                        <li><a class="mdi mdi-face-profile tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Professor" href="{{url(route("professor.index"))}}">Professor</a></li>
                        <li><a class="mdi mdi-note tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Notas" href="{{url(route("nota.index"))}}">Notas</a></li>
                        <li><a class="mdi mdi-face tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Aluno" href="{{url(route("aluno.index"))}}">Aluno</a></li>
                        <li><a class="mdi mdi-alphabetical tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Turma" href="{{url(route("turma.index"))}}">Turma</a></li>
                        <li><a class="mdi mdi-calendar tooltipped" data-position="botton" data-delay="50"
                               data-tooltip="Horario" href="{{url(route("horario.index"))}}">Horario</a></li>

                    @endif


                </div>
            </ul>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{url("materialize/js/materialize.js")}}" type="text/javascript"></script>
<script src="{{url("/js/utils-materialize.js")}}" type="text/javascript"></script>

<script>
    $(".button-collapse").sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
    });
</script>
</body>
<style type="text/css">
    .brand-logo {
        font-weight: 900;
    }

    .dropdown-content {
        background-color: #FFFFFF;
        margin: 0;
        display: none;
        min-width: 300px; /* Changed this to accomodate content width */
        max-height: auto;
        margin-left: -1px; /* Add this to keep dropdown in line with edge of navbar */
        overflow: hidden; /* Changed this from overflow-y:auto; to overflow:hidden; */
        opacity: 0;
        position: absolute;
        white-space: nowrap;
        z-index: 1;
        will-change: width, height;
    }
</style>
</html>
