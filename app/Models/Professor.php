<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Professor extends Model
{
    protected $table = 'professors';
    protected $fillable = ['nome', 'cpf', 'bairro', 'naturalidade', 'nascionalidade', 'sexo', 'email', 'ctps', 'data_emissao', 'profissao', 'data_nascimento', 'estado_civil', 'numero_residencia', 'orgao_emissor', 'pis_pasep', 'pos_graduacao', 'cidade', 'etinia', 'grau_instrucao', 'rg', 'celular', 'endereco', 'secao',
    'serie', 'situacao', 'titulo_eleitor', 'estado', 'complemento', 'cep'];

    public $rules = ['name' => 'required|max:255'];

    public function horarios(): HasMany
    {
        return $this->hasMany(Horario::class, 'id_professor');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
