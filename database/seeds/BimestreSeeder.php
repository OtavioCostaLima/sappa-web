<?php

use Illuminate\Database\Seeder;
use App\Models\Bimestre;

class BimestreSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bimestre::insert([
            'descricao' => '1 BIMESTRE']);
        Bimestre::insert([
            'descricao' => '2 BIMESTRE']);
        Bimestre::insert([
            'descricao' => '3 BIMESTRE']);
        Bimestre::insert([
            'descricao' => '4 BIMESTRE']);


    }

}
