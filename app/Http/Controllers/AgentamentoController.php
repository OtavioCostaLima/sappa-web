<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agendamento;
use App\Models\Equipamento;
use App\Models\Professor;

class AgentamentoController extends Controller
{
  public function agendar(Request $request){
      $agendamento = new Agendamento();
      //ver o numero de equipamentos dsponiveispara o rorario agendado
      /* select equipamento.descricao from equipamento
      join agendamento on (equipamento.id=agendamento.equipamento_id)
      where (($hora_inicio < hora_inicio and $hora_termino < hora_inicio) or
      ($hora_inicio > hora_termino and $hora_termino < hora_termino)) and $data_agendamento = data_agendamento

        */
        $agendamento->create([
        'professor_id' => $request->professor_id,
        'equipamento_id' => $request->equipamento_id,
        'data_agendamento' => $request->data_agendamento,
        'hora_inicio' => $request->hora_inicio,
        'hora_termino' => $request->hora_termino,
        'quantidade' => $request->quantidade
      ]);
  }
}
